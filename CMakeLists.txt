cmake_minimum_required(VERSION 3.17)
project(Lab_1_10_02_2021)

set(CMAKE_CXX_STANDARD 20)

add_executable(Lab_1_10_02_2021 main.cpp)