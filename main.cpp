#include <iostream>
#include <fstream>
#include <random>

using namespace std;

int main() {
    fstream file;
    string line;
    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<int> dist(1, 10000);

    file.open("random.txt", ios::in);
    if (file.peek() != EOF) {
        cout << "Poprzednie losowo wygenerowane liczby: \n";
        while (getline(file, line)) {
            cout << line << "\n";
        }
    } else {
        cout << "PLik \"random.txt\" jest pusty. \n";
    }

    file.close();
    file.open("random.txt", ios::out);
    for (int i = 0; i < 18; i++) {
        file << dist(mt) << "\n";
    }
    file.close();
    return 0;
}
